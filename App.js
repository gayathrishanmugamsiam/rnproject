import React, { Component } from 'react';
import { View, Text } from 'react-native';
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={[{flexDirection:'row'}, styles.elementsContainer]}>
          <View style={{flex: 1, backgroundColor: 'pink'}} />
          <View style={{flex: 1, backgroundColor: 'blue'}} />
        </View>
        <View style={[{flex: 1}, styles.elementsContainer]}>
          <View style={{flex: 1, backgroundColor: 'yellow'}} />
          <View style={{flex: 1, backgroundColor: 'green'}} />
          <View style={{flex: 1, backgroundColor: 'red'}} />

        </View>
      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1
  },
  elementsContainer: {
    flex: 1,
  }
}