import React, { Component } from 'react';
import { View, Text, Image,SafeAreaView,StyleSheet, TouchableOpacity } from 'react-native';

export default class Create extends Component {
    render() {
        return (
            <SafeAreaView style = {styles.container}>
            <View style={styles.boxcontainer}>
            <Image 
            style={{height:350, width:320}}
            source={require('../../assets/image.png')}
            />
            </View>
        
            <View style={styles.boxContainer}>
                <Text style={ {fontSize:40, fontWeight:900}}>Hello!</Text>
                <Text style={{fontSize:15, margin:10}}>Best place to Write life stories and {"/n"}share your journey experince</Text> 

                <TouchableOpacity>
                    <Text style={[styles.button,{marginTop:45, fontSize:16, color: 'black',backgroundColor:'blue'}]}>LOGIN</Text>
                </TouchableOpacity>
            

                <TouchableOpacity>
                    <Text style={[styles.button,{marginTop:45, fontSize:16, color: 'black',backgroundColor:'white'}]}>SIGNUP</Text>
                </TouchableOpacity>
                </View>
                </SafeAreaView>
                
            );
            }
            }

            const styles=StyleSheet.create({

                container: {
                    flex: 1

                },
                boxContainer: {
                    flex:1,
                    backgroundColor: 'white',
                    alignItems: 'center'

                },
                text:{
                    fontSize:20,
                    textAlign:'center',
                    color: 'black'
                },
                button: {
                    borderRadius: 5,
                    borderWidth:2,
                    padding :5,
                    textAlign: 'center',
                    height:40,
                    width: 200,
                    bordercolor:'black'
                
                }
            });
                      
            
