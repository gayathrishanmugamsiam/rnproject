/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Create from './src/new/Create';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Create);
